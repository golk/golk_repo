import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

# Reading data
data = pd.read_csv(
    '/home/golk/ex1data1.txt',
    header=None,
    names=['x1', 'y']
)

# Adding X0
data['x0'] = 1

# Dataframe to Numpy Array
x = np.array(data[['x0', 'x1']])
y = np.array(data['y'])


def linear_regression_model(x, y):
    # Initialize Theta
    theta = np.array([0, 0])

    # Gradient Descent (Full Batch) constants
    alpha = 0.01
    num_iter = 10 ** 4
    stop_case = 10 ** -5

    for iter in np.arange(num_iter):
        # Hypothesis: Linear Regression
        h1 = np.dot(x, theta.transpose())

        # Cost function before updating thetas
        J1 = 1/2 * sum(np.power(h1 - y, 2))

        # Updating all thetas (Simultaneously)
        theta = np.array([theta[i] - alpha * np.dot((h1-y), x[:, i]) for i in np.arange(theta.size)])

        # Cost function after updating thetas
        h2 = np.dot(x, theta.transpose())
        J2 = 1/2 * sum(np.power(h2 - y, 2))

        # Exiting loop when converges
        if abs(J2 - J1) <= stop_case:
            print(
                'Converged at {0}/{1}'.format(iter + 1, num_iter),
                '| a:', alpha,
                '| Cost:', round(J2, 4),
                '| Thetas:', theta.round(4),
                '| Stop case:', stop_case
            )
            return h2

        # Exiting loop when diverges
        if J2 > J1:
            alpha = alpha * 0.1
            print('Alpha too large, now:', alpha)

    # Returning alarm
    if abs(J2 - J1) > stop_case:
        print(
            "Didn't converge during {0}/{1}".format(iter + 1, num_iter),
            '| a:', alpha,
            '| Cost:', round(J2, 4),
            '| Thetas:', theta.round(4),
            '| Stop case:', stop_case, '\n'
        )

predicted_y = linear_regression_model(x, y)

# Plotting results
plt.figure(1)

plt.subplot(211)
plt.plot(x[:, 1], y, 'go')
plt.title(r'Linear Regression')
plt.grid(True); plt.ylabel('Y')

plt.subplot(212)
plt.plot(x[:, 1], y, 'gx')
plt.plot(x[:, 1], predicted_y, 'bo')
plt.grid(True); plt.xlabel('X'); plt.ylabel('Predicted Y')
plt.show()
